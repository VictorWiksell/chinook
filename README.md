# chinook
Chinook is a spring boot application with postgresql which is a relational database. 

## Contributors
@VictorWiksell @denise.leinonen @nicolaspalaciosexperisgitlab

## Installation

Git clone this project to get started. We used Java 18 for this application so make sure you have updated JDK. 


## Usage
To test out the functionality, go to CustomerRunner.java, add System.out.println() <-- inside parameters add customerRepository. and
the name of the method you want to use. You can find all the methods inside CustomerRepositoryImpl.java. For some methods, you need to fill in parameters, if your using intellij you can do CTRL p or go to CustomerRepositoryImpl to see what parameters the method requests. Then go to ChinookApplication.java and run the program. Example of how to print out a function:
System.out.println(customerRepository.findAll());

## Contributing
Pull requests are welcome. For major changes, please contact us and discuss your ideas.
