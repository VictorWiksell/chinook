package com.example.chinook.repository;

import com.example.chinook.models.Customer;
import com.example.chinook.models.CustomerCountry;
import com.example.chinook.models.CustomerGenre;
import com.example.chinook.models.CustomerSpender;

import java.util.List;

/**
 * Custom customer repository
 *
 * @author Nicolas Palacios
 *
 * extends generic CRUD repository
 */

public interface CustomerRepository extends CrudRepository<Customer, Integer> {
    CustomerCountry mostMembersCountry();

    /**
     *Custom method returns a lis of customers with given parameters
     *
     * @param limit where the list starts
     * @param offset how many in customers in the list
     *
     * @return a list of customers with given parameters
     * @throws RuntimeException
     */
    List<Customer> findLimitCustomers( int limit, int offset);

    /**
     * Custom method returns a list of customer matching given first name
     *
     * @param firstName takes a parameter of type String
     * @return a list of customers matching the given name
     * @throws RuntimeException
     */
    List<Customer> findCustomerByFirstName(String firstName);

    /**
     * Custom method returns the mos popular genre for given customer, gives two records if it's a tie
     *
     * @param customerId takes a parameter of tye int
     * @return a list with a record with a customers favourite genre, gives two records i it's a tie
     * @throws RuntimeException
     */
    List<CustomerGenre> findMostPopularGenre(int customerId);

    /**
     * Custom method returns the customer who spent the most on music records
     *
     * @return a list of the customers who spent the most on music records with a limit to on record
     * @throws RuntimeException
     */
    List<CustomerSpender> highestSpender();
}
