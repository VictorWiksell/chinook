package com.example.chinook.repository;

import com.example.chinook.models.Customer;
import com.example.chinook.models.CustomerCountry;
import com.example.chinook.models.CustomerGenre;
import com.example.chinook.models.CustomerSpender;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class CustomerRepositoryImpl implements CustomerRepository {
    private String url;
    private String username;
    private String password;

    public CustomerRepositoryImpl(
            @Value("${spring.datasource.url}") String url,
            @Value("${spring.datasource.username}") String username,
            @Value("${spring.datasource.password}") String password) {
        this.url = url;
        this.username = username;
        this.password = password;
    }

    @Override
    public List<Customer> findAll() {
        List<Customer> customerList = new ArrayList<>();

        String sql = "SELECT * FROM Customer";
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                customerList.add(new Customer(
                        resultSet.getInt("customer_id"),
                        resultSet.getString("first_name"),
                        resultSet.getString("last_name"),
                        resultSet.getString("country"),
                        resultSet.getString("postal_code"),
                        resultSet.getString("phone"),
                        resultSet.getString("email")
                ));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customerList;
    }

    @Override
    public Customer findById(Integer id) {
        Customer customer = null;
        String sql = "SELECT * FROM customer WHERE customer_id = ?";
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                customer = new Customer(
                        resultSet.getInt("customer_id"),
                        resultSet.getString("first_name"),
                        resultSet.getString("last_name"),
                        resultSet.getString("country"),
                        resultSet.getString("postal_code"),
                        resultSet.getString("phone"),
                        resultSet.getString("email")
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customer;
    }

    @Override
    public List<Customer> findCustomerByFirstName(String firstName) {
        List<Customer> customerList = new ArrayList<>();
        String sql = "SELECT * FROM customer WHERE first_name LIKE ?";
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setString(1, "%" + firstName + "%");
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                customerList.add(new Customer(
                        resultSet.getInt("customer_id"),
                        resultSet.getString("first_name"),
                        resultSet.getString("last_name"),
                        resultSet.getString("country"),
                        resultSet.getString("postal_code"),
                        resultSet.getString("phone"),
                        resultSet.getString("email"))
                );
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return customerList;
    }

    @Override
    public List<Customer> findLimitCustomers(int limit, int offset) {
        List<Customer> customerList = new ArrayList<>();
        String sql = "SELECT * FROM Customer LIMIT ? OFFSET ?";
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setInt(1, limit);
            preparedStatement.setInt(2, offset);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                customerList.add(new Customer(
                        resultSet.getInt("customer_id"),
                        resultSet.getString("first_name"),
                        resultSet.getString("last_name"),
                        resultSet.getString("country"),
                        resultSet.getString("postal_code"),
                        resultSet.getString("phone"),
                        resultSet.getString("email")
                ));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customerList;
    }

    @Override
    public int insert(Customer object) {
        var sql = "INSERT INTO Customer(first_name, last_name, country, postal_code, phone, email) VALUES (?,?,?,?,?,?)";
        int rowsAffected = 0;
        try (var conn = DriverManager.getConnection(url, username, password)) {
            var preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setString(1, object.firstName());
            preparedStatement.setString(2, object.lastName());
            preparedStatement.setString(3, object.country());
            preparedStatement.setString(4, object.postalCode());
            preparedStatement.setString(5, object.phone());
            preparedStatement.setString(6, object.email());
            rowsAffected = preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rowsAffected;
    }

    @Override
    public int update(Customer object) {
        var sql = "UPDATE customer  SET first_name = ?, last_name = ?, country = ?, postal_code = ?, phone = ? , email = ? WHERE customer_id = ?";
        int rowsAffected = 0;
        try (var conn = DriverManager.getConnection(url, username, password)) {
            var preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setString(1, object.firstName());
            preparedStatement.setString(2, object.lastName());
            preparedStatement.setString(3, object.country());
            preparedStatement.setString(4, object.postalCode());
            preparedStatement.setString(5, object.phone());
            preparedStatement.setString(6, object.email());
            preparedStatement.setInt(7, object.id());
            rowsAffected = preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rowsAffected;
    }

    @Override
    public CustomerCountry mostMembersCountry() {
        CustomerCountry customer = null;
        String sql = "SELECT country, count(*) AS country_most_members FROM customer GROUP BY country ORDER BY country_most_members DESC limit 1";
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                customer = new CustomerCountry(
                        resultSet.getString("country"),
                        resultSet.getInt("country_most_members")
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customer;
    }

    @Override
    public List<CustomerSpender> highestSpender() {
        List<CustomerSpender> highestSpender = new ArrayList<>();
        String sql = "SELECT c.customer_id, c.first_name, c.last_name, SUM(inv.total) AS highest_spender FROM invoice inv INNER JOIN customer AS c ON inv.customer_id = c.customer_id GROUP BY c.customer_id, c.first_name, c.last_name ORDER BY highest_spender DESC LIMIT 1";
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                highestSpender.add(new CustomerSpender(
                        resultSet.getInt("customer_id"),
                        resultSet.getString("first_name"),
                        resultSet.getString("last_name"),
                        resultSet.getString("highest_spender")
                ));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return highestSpender;
    }

    @Override
    public List<CustomerGenre> findMostPopularGenre(int customerId) {
        List<CustomerGenre> customerList = new ArrayList<>();
        String sql = "SELECT c.customer_id, c.first_name, c.last_name, g.name, COUNT(g.name) AS most_popular_genre " +
                "FROM customer c INNER JOIN invoice AS inv ON inv.customer_id = c.customer_id INNER JOIN invoice_line AS il ON il.invoice_id = inv.invoice_id " +
                "INNER JOIN track AS t ON t.track_id = il.track_id INNER JOIN genre AS g ON g.genre_id = t.genre_id " +
                "WHERE c.customer_id = ? GROUP BY c.customer_id, c.first_name, c.last_name, g.name " +
                "ORDER BY most_popular_genre DESC FETCH FIRST 1 ROWS WITH TIES";
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setInt(1, customerId);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                customerList.add(new CustomerGenre(
                        resultSet.getInt("customer_id"),
                        resultSet.getString("first_name"),
                        resultSet.getString("last_name"),
                        resultSet.getString("name"),
                        resultSet.getInt("most_popular_genre")
                ));
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return customerList;
    }

    @Override
    public int delete(Customer object) {
        return 0;
    }

    @Override
    public int deleteById(Integer integer) {
        return 0;
    }
}
