package com.example.chinook.repository;

import java.sql.SQLException;
import java.util.List;

/**
 * Generic CRUD repository
 *
 *@author Nicolas Palacios
 *
 * @param <T>  generic parameter T symbolises an object
 * @param <ID> generic parameter ID symbolises a datatype
 *
 */
public interface CrudRepository<T, ID> {

    /**
     * Generic method to get all customers
     *
     * @return all the cutomers, returns objects
     * @throws RuntimeException
     */
    List<T> findAll();

    /**
     * Generic method gets customer by id
     *
     * @param id takes a parameter datatype int
     * @return given customer by id, returns an object
     * @throws RuntimeException
     */
    T findById(ID id);

    /**
     * Generic method gets customer by name
     *
     * @param String takes a parameter datatype String
     * @return user with given name, returns an object
     */
//    T findByUsername(T String);

    /**
     * Generic method inserts a customer
     *
     * @param object takes a parameter of object type
     * @return returns null
     */
    int insert(T object);

    /**
     * Generic method updates a customer
     *
     * @param object takes a parameter of object type
     * @return returns null
     * @throws RuntimeException
     */
    int update(T object);

    /**
     * Generic method deletes on given instructions
     *
     * @param object takes a parameter of type object
     * @return null
     */
    int delete(T object);

    /**
     * Generic method deletes customer by id
     *
     * @param id takes a parameter if int type
     * @return null
     */
    int deleteById(ID id);
}
