package com.example.chinook.runner;

import com.example.chinook.models.Customer;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import com.example.chinook.repository.CustomerRepository;

/**
 * Custom runner
 *
 * @author Nicolas Palacios
 *
 * runs methods and leaves main() method clean from unnecessary code.
 */
@Component
public class CustomerRunner implements ApplicationRunner {
    private final CustomerRepository customerRepository;

    public CustomerRunner(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    /**
     * Customer runner runs all the given methods
     * @param args takes in different params depending on method
     * @throws Exception sql exception
     */
    @Override
    public void run(ApplicationArguments args) throws Exception {
        // find all
        System.out.println(customerRepository.findAll());

        // find by id
        System.out.println(customerRepository.findById(50));

        // find customer by name
        System.out.println(customerRepository.findCustomerByFirstName("Frank"));

        // set limit list
        System.out.println(customerRepository.findLimitCustomers(10, 2));

        // inserts new customer
        customerRepository.insert(new Customer(0, "Benny", "Headbanger", "Sweden", "195", "07388754", "benny195@gmail.com"));

        //update customer and retrieves the updated one
        customerRepository.update(new Customer(60, "Up Benny", "Up Headbanger", "Sweden", "Up 195", "Up 07388754", "upbenny195@gmail.com"));
        System.out.println(customerRepository.findById(60));

        // most members per country
        System.out.println(customerRepository.mostMembersCountry());

        // finds highest spender
        System.out.println(customerRepository.highestSpender());

        // finds most popular genre for spec customer
        System.out.println(customerRepository.findMostPopularGenre(12));
    }
}
