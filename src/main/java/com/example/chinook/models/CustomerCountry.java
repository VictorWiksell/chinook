package com.example.chinook.models;

/**
 * Represent a customers country
 *
 * @author Nicolas Palacios
 *
 * @param country customers country
 * @param members customers that are from the same country
 */
public record CustomerCountry(String country, int members) {
}
