package com.example.chinook.models;

/**
 * Represents highest spender
 *
 * @author Nicolas Palacios
 *
 * @param customer_id unique customer identifier
 * @param first_name customers first name
 * @param last_name customers last name
 * @param total customers total spending on music records
 */
public record CustomerSpender(int customer_id, String first_name, String last_name, String total) {
}

