package com.example.chinook.models;

/**
 *Represents a customers music genre
 *
 * @author Nicolas Palacios
 *
 * @param customerId unique identifier for a given customer
 * @param firstName customers first name
 * @param lastName customers last name
 * @param genre customers music genre
 * @param mostPopular represents customers most favourite genre
 */
public record CustomerGenre(int customerId, String firstName, String lastName, String genre, int mostPopular) {
    @Override
    public String toString() {
        return "  CustomerGenre " + '\n' +
                " customerId= " + customerId + '\n' +
                " firstName= " + firstName + '\'' + '\n' +
                " lastName= " + lastName + '\'' + '\n' +
                " genre= " + genre + '\'' + '\n' +
                " mostPopular= " + mostPopular + '\n';
    }
}
