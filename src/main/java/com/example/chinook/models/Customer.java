package com.example.chinook.models;

/**
 * Represents a customer. Is the main entity we'll be using for this application.
 *
 * @author Nicolas Palacios
 *
 * @param id unique identifier for the customers
 * @param firstName customers first name
 * @param lastName customers last name
 * @param country customers country
 * @param postalCode customers postal code
 * @param phone customers phone
 * @param email customers email
 */
public record Customer(int id, String firstName, String lastName,String country, String postalCode, String phone, String email) {
}
